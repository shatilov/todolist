<?php
/**
 * Класс задач пользователя
 */
class Task {
    private $dbh;
 
    function __construct($dbh) {
      $this->dbh = $dbh;
    }
	
	private function prepareData($fields) {
		$date = new DateTime($fields['datetime']);
		$datetime = $date->format('Y-m-d H:i:s');
		
		$result = array(
			'user_id' => 10,
			'title' => trim(strip_tags(($fields['title']))),
			'description' => trim(strip_tags(($fields['description']))),
			'completed' => filter_var($fields['completed'], FILTER_VALIDATE_BOOLEAN),
			'datetime' => $datetime
		);
		
		return $result;
	}
 
    public function create($fields) {
		try {
			$stmt = $this->dbh->prepare("INSERT INTO task(user_id, title, description, completed, datetime) VALUES(:user_id, :title, :description, :completed, :datetime)");

			$data = $this->prepareData($fields);
			$stmt->execute($data); 

			return $stmt; 
		} catch(PDOException $e) {
			echo $e->getMessage();
		}    
    }
 
    /*public function login($fields) {
		try {
			$stmt = $this->dbh->prepare("SELECT * FROM user WHERE email=:email LIMIT 1");
			$stmt->execute(array(
				':email' => $fields['email']
			));
			$userRow = $stmt->fetch(PDO::FETCH_ASSOC);
			
			if($stmt->rowCount() > 0) {
				if(password_verify($fields['password'], $userRow['password'])) {
					$_SESSION['user_session'] = $userRow['id'];
					$_SESSION['user']['name'] = $userRow['name'];
					$_SESSION['user']['email'] = $userRow['email'];
					return true;
				} else {
					return false;
				}
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
    }
 
	public function isLoggedin() {
		if(isset($_SESSION['user_session'])) {
			return true;
		}
	}
	
	public function getName() {
		if(isset($_SESSION['user_session'])) {
			return $_SESSION['user']['name'];
		}
	}

	public function getEmail() {
		if(isset($_SESSION['user_session'])) {
			return $_SESSION['user']['email'];
		}
	}
 
	public function redirect($url) {
	   header("Location: $url");
	}

	public function logout() {
		session_destroy();
		unset($_SESSION['user_session']);
		unset($_SESSION['user']);
		return true;
	}*/
}