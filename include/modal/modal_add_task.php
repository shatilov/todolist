<!-- Модальное окно регистрации -->
<div class="modal fade" id="modal_add_task" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Добавить задачу</h4>
      </div>
      <div class="modal-body">
		<div class="">
			<form class="form-horizontal" method="POST" action="/tasks.php">
				
				<div class="form-group">
					<label for="title" class="control-label">Заголовок</label>
					<div class="input-group">
						<input pattern=".{2,}" required title="Заголовок должен содержать не менее 2 символов" type="text" class="form-control" name="title" placeholder="Введите Заголовок"/>
					</div>
				</div>
				
				<div class="form-group">
					<label for="title" class="control-label">Дата/время</label>
					<div class="input-group">
						<input pattern=".{2,}" required title="Заголовок должен содержать не менее 2 символов" type="date" class="form-control" name="datetime" placeholder="Введите Заголовок"/>
					</div>
				</div>
				
				<div class="form-group">
					<label for="title" class="control-label">Текст задачи</label>
					<div class="input-group">
						<textarea name="description" class="form-control" placeholder="Введите Текст задачи"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<div class="input-group">
						<input type="checkbox" name="completed" checked/>
					</div>
				</div>

				<div class="form-group ">
					<button type="submit" name="add_task" class="btn btn-danger btn-lg btn-block login-button">Добавить</button>
				</div>
			</form>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- /Модальное окно регистрации -->