<?php include_once( __DIR__ . '/include/header.php' ); ?>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1>ToDoList - Достигайте большего, каждый день!</h1>
        <p>Управляйте вашими задачами и проектами где угодно с помощью ToDoList. Дома. На работе. Онлайн. Оффлайн.</p>
        
		<?php if (!$user->isLoggedin()) { ?>
		<p><a class="btn btn-danger btn-lg" href="#" role="button" data-toggle="modal" data-target="#modal_registration">Начать прямо сейчас &raquo;</a></p>
		<?php } else { ?>
		<p><a class="btn btn-danger btn-lg" href="/tasks.php" role="button">Перейти к задачам&raquo;</a></p>
		<?php } ?>
    </div>

    
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Дизайн без отвлекающих элементов</h2>
          <p>Ваши задачи – большая часть вашей жизни. Оставайтесь мотивированными и организованными с помощью прекрасного, интуитивного списка задач ToDoList.</p>
          <p><a class="btn btn-default" href="#" role="button">Подробнее &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Дизайн без отвлекающих элементов</h2>
          <p>Ваши задачи – большая часть вашей жизни. Оставайтесь мотивированными и организованными с помощью прекрасного, интуитивного списка задач ToDoList.</p>
          <p><a class="btn btn-default" href="#" role="button">Подробнее &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Дизайн без отвлекающих элементов</h2>
          <p>Ваши задачи – большая часть вашей жизни. Оставайтесь мотивированными и организованными с помощью прекрасного, интуитивного списка задач ToDoList.</p>
          <p><a class="btn btn-default" href="#" role="button">Подробнее &raquo;</a></p>
        </div>
      </div>
      <hr>
	  
<?php include_once( __DIR__ . '/include/footer.php' ); ?>