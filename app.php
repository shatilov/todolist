﻿<?php
	require_once ( $_SERVER['DOCUMENT_ROOT'] . "/lib/database.php");
	require_once ( $_SERVER['DOCUMENT_ROOT'] . "/lib/TreeMenu.php");
	
	$treeMenu = new TreeMenu($dbh);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>ToDoList</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/registration.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Регистрация</h4>
      </div>
      <div class="modal-body">
		<div class="main-login main-center">
			<form class="form-horizontal" method="POST" action="#">
				
				<div class="form-group">
					<label for="name" class="cols-sm-2 control-label">Имя</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
							<input required type="text" class="form-control" name="user_name" id="user_name"  placeholder="Введите Ваше имя"/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="email" class="cols-sm-2 control-label">Email</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
							<input required type="email" class="form-control" name="user_email" id="user_email"  placeholder="Введите Ваш Email"/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="mobile" class="cols-sm-2 control-label required">Пароль</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock aria-hidden="true"></i></span>
							<input required type="password" class="form-control" name="user_pass" id="user_pass"  placeholder="Придумайте пароль"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="mobile" class="cols-sm-2 control-label required">Подтверждение пароля</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock aria-hidden="true"></i></span>
							<input requireds type="password" class="form-control" name="user_pass_repeat" id="user_pass_repeat"  placeholder="Повторите пароль"/>
						</div>
					</div>
				</div>

				<div class="form-group ">
					<button type="submit" class="btn btn-danger btn-lg btn-block login-button">Зарегистрироваться</button>
				</div>
			</form>
			<div class="text-center">
				<a href="#">Уже зарегистрированы?</a>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
  


    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">ToDoList</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <!--ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Action</a></li>
					<li><a href="#">Another action</a></li>
				</ul>
            </li>
          </ul-->
		  <ul class="nav navbar-nav">
			<?php $treeMenu->getMenuHtml(); ?>
          </ul>
		
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Введите email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Введите пароль" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Войти</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>ToDoList - Достигайте большего, каждый день!</h1>
        <p>Управляйте вашими задачами и проектами где угодно с помощью ToDoList. Дома. На работе. Онлайн. Оффлайн.</p>
        <p><a class="btn btn-danger btn-lg" href="#" role="button" data-toggle="modal" data-target="#myModal">Начать прямо сейчас &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Дизайн без отвлекающих элементов</h2>
          <p>Ваши задачи – большая часть вашей жизни. Оставайтесь мотивированными и организованными с помощью прекрасного, интуитивного списка задач ToDoList.</p>
          <p><a class="btn btn-default" href="#" role="button">Подробнее &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Дизайн без отвлекающих элементов</h2>
          <p>Ваши задачи – большая часть вашей жизни. Оставайтесь мотивированными и организованными с помощью прекрасного, интуитивного списка задач ToDoList.</p>
          <p><a class="btn btn-default" href="#" role="button">Подробнее &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Дизайн без отвлекающих элементов</h2>
          <p>Ваши задачи – большая часть вашей жизни. Оставайтесь мотивированными и организованными с помощью прекрасного, интуитивного списка задач ToDoList.</p>
          <p><a class="btn btn-default" href="#" role="button">Подробнее &raquo;</a></p>
        </div>
      </div>

	  
	  
      <hr>

      <footer>
        <p>&copy; 2016 Company, Inc.</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/vendor/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/vendor/bootstrap.min.js"></script>
  </body>
</html>
